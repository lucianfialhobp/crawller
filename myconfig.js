// Render Multiple URLs to file

var RenderUrlsToFile, arrayOfUrls, system;

system = require("system");

/*
Render given urls
@param array of URLs to render
@param callbackPerUrl Function called after finishing each URL, including the last URL
@param callbackFinal Function called after finishing everything
*/
RenderUrlsToFile = function(urls, callbackPerUrl, callbackFinal) {
    var getFilename, next, page, retrieve, urlIndex, webpage;
    urlIndex = 0;
    webpage = require("webpage");
    page = null;
    getFilename = function() {
        return "rendermulti-" + urlIndex + ".png";
    };
    next = function(status, url) {
        page.close();
        callbackPerUrl(status, url);
        return retrieve();
    };
    retrieve = function() {
        var url;
        if (urls.length > 0) {
            url = urls.shift();
            console.log(url);

            urlIndex++;
            page = webpage.create();
            page.viewportSize = {
                width: 800,
                height: 600
            };
            page.settings.userAgent = "Phantom.js bot";
            
            return page.open("http://" + url, function(status) {
                var arrayData = {};

                if (status === "success") {
                    return window.setTimeout((function() {
                        var ua = page.evaluate(function() {
                            return document.querySelectorAll("#header h1 a")[0].innerHTML;
                        });

                        console.log(ua);

                        return next(status, url);

                    }), 200);
                } else {
                    return next(status, url);
                }
            });
        } else {
            return callbackFinal();
        }
    };
    return retrieve();
};



function SomarData(xData,DiasAdd) 
{
        var d = new Date();
         d.setTime(Date.parse(xData.split("-").reverse().join("-"))+(86400000*(DiasAdd)))
        var DataFinal;
        if(d.getDate() < 10)
        {
            DataFinal = "0"+d.getDate().toString();
        }
        else
        {    
            DataFinal = d.getDate().toString();    
        }
        
        if((d.getMonth()+1) < 10){
            DataFinal += "/0"+(d.getMonth()+1).toString()+"/"+d.getFullYear().toString();
        }
        else
        {
            DataFinal += "/"+((d.getMonth()+1).toString())+"/"+d.getFullYear().toString();
        }
        return DataFinal;
}

function addDayIntoDate(data_inicial,dias){
    
    data_day = data_inicial.getDay();

    data_mais = data_inicial+dias;

    return data_mais;
};

montaUrls = function(intervalo){
        origens = array("POA","FLN","CWB","CGH","CPQ","GIG","PLU","BSB","VIX","SSA","REC","GYN","CGR","MCZ","FOR","JPA","NAT","SLZ","BEL","MAO");

        destinos = array("POA","FLN","CWB","CGH","CPQ","GIG","PLU","BSB","VIX","SSA","REC","GYN","CGR","MCZ","FOR","JPA","NAT","SLZ","BEL","MAO"
        ,"MIA","ORL","HOU","DAL","SAN","LAX","SFO","SEA","DTW","MDW","ATL","DCA","PHL","JFK","BOS","YYZ","YMX","YVR","MEX","HAV","PTY","CUN"
        ,"CCS","LIM","BOG","UIO","SCL","LPB","ASU","AEP","MVD","ORY","STN","CIA","LIN","BCN","MAD","SXF","HAM","DUB","LIS","FBU","ARN","BRN"
        ,"ZRH","DME","PRY","CAI","CPT","PVG","BKK","NRT","HKG","CGK","KUL","SEL","SYD","TPE","DEL","RUH","JRS","AUH","DOH","DXB","WAW","PRG"
        ,"RBA");

        dt_inicial = new Date();

        t_origens = count($origens);
        i=0;
        j=0;
        urls= array();

        if(intervalo == 30){
            for (i=0; i < origens.length; i++){
                for (j=0; j < destinos.length; j++){
                    for(w=0; w < 6; w++){
                        if(origens[i]!=destinos[j]){
                            data = "http://passagem.viajarbarato.com.br/Vuelos/1/".origens[i]."/".destinos[j]."/".dt_inicial."/".addDayIntoDate(dt_inicial,30)."/1/0/0/_/Economica/false";
                            urls[] = $html;
                            dt_inicial = addDayIntoDate(dt_inicial,30);
                        }
                    }
                    dt_inicial = date("d")."/".date("m")."/".date("Y");
                }
            }
        }


    return data;  
};

arrayOfUrls = null;

if (system.args.length > 1) {
    arrayOfUrls = Array.prototype.slice.call(system.args, 1);
} else {
    console.log("Usage: phantomjs render_multi_url.js [domain.name1, domain.name2, ...]");
    arrayOfUrls = montaUrls();
}

RenderUrlsToFile(arrayOfUrls, (function(status, url, file) {
    
    if (status !== "success") {
        return console.log("Unable to render '" + url + "'");
    } else {
        return console.log("Rendered '" + url + "' at '" + file + "'");
    }
}), function() {
    return phantom.exit();
});